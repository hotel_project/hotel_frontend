import { ContentObserver } from '@angular/cdk/observers';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking, Room } from '../Data/Room';
import { CredentialsService } from '../services/credentials.service';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'app-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.less']
})
export class RoomDetailsComponent implements OnInit {

  @Input() roomData: Room = new Room();

  minDate: Date = new Date();

  public isLogged() {
    return this.authService.logInfo.logged;
  }

  bookingDate: Date[] = [];

  get f() { return this.dateRange.controls; }

  dateRange = new FormGroup({
    start: new FormControl('', [Validators.required]),
    end: new FormControl('', [Validators.required])
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private roomService: RoomsService,
    private authService: CredentialsService) {
    let roomid = Number.parseInt(this.route.snapshot.paramMap.get('roomId') || '-1');
    this.roomService.getRoom(roomid).subscribe(
      res => {
        this.roomData = res;
      },
      err => {
        this.router.navigate(["room_list"]);
      }
    );
    this.roomService.getRoomReservations(roomid).subscribe(
      res => {
        this.feelCalendar(res);
      },
      err => {
      }
    )
    var minCurrentDate = new Date();
    this.minDate = minCurrentDate;
  }

  ngOnInit(): void {
  }


  validDate: boolean = false;

  checkDateRange(){
    let range = this.getDates(this.dateRange.value.start, this.dateRange.value.end);
    for (let index = 0; index < this.bookingDate.length; index++) {
      let matched = range.find((x) => x.getDate() == this.bookingDate[index].getDate());
      if(matched != null){
        this.validDate = true;
        return;
      }
    }
    this.validDate  = false;
  }

  feelCalendar(booking: Booking[]) {
    booking.forEach(book => {
      let range = this.getDates(new Date(book.d_start), new Date(book.d_end));
      this.bookingDate.push(...range);
    });
  }

  getDates(startDate: Date, stopDate: Date): Date[] {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
      dateArray.push(new Date(currentDate));
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return dateArray;
  }

  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).toDateString();
    return this.bookingDate.findIndex(testDate => day === testDate.toDateString()) == -1;
  }

  submitBooking() {
    /// toISOString set down to 1 day... no idea why
    this.dateRange.value.start.setDate(this.dateRange.value.start.getDate() + 1);
    this.dateRange.value.end.setDate(this.dateRange.value.end.getDate() + 1);
    let startingDate = this.dateRange.value.start.toISOString().slice(0, 19).replace('T', ' ');
    let endDate = this.dateRange.value.end.toISOString().slice(0, 19).replace('T', ' ');
    let bookingData = new Booking(startingDate, endDate, this.roomData._id, this.authService.credential._id);
    this.roomService.submitBooking(bookingData).subscribe();
  }
}
