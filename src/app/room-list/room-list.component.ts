import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../services/rooms.service';
import { Room } from '../Data/Room';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.less']
})
export class RoomListComponent implements OnInit {

  errorMessage = '';
  rooms: Room[] = [];

  constructor(
    private roomService: RoomsService) { }

  ngOnInit(): void {
    this.requerRooms();
  }

  requerRooms(){
    this.roomService.getRooms().subscribe(
      res =>{
        this.rooms = res;
      },
      err=>{
        this.errorMessage = err;
      }
    )
  }
}
