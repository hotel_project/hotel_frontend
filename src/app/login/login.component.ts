import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CredentialsService } from '../services/credentials.service';
import { ValidationService } from '../services/validation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  messageRespond : String = "";
  loginForm : FormGroup = new FormGroup({});
  returnUrl: string = "home";
  forgotPassword : string = "forgot-password"
  formErrors: any;
  submitted = false;

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  constructor(
    private router: Router,
    private fb: FormBuilder,
    public vs: ValidationService,
    private route: ActivatedRoute,
    private authService: CredentialsService) {

  }

  ngOnInit(): void {
    let tmpReturnUrl = this.route.snapshot.queryParams["returnUrl"];
    if (tmpReturnUrl != undefined)
    {
      this.returnUrl = tmpReturnUrl;
    }
    this.formErrors = this.vs.errorMessages;
    this.createForm();
  }

  onSubmit(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.login();
    this.messageRespond = "";
  }

  login (){
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe(
      res => {
        this.getUserData();
      },
      err =>{
        this.messageRespond = "Login fail, check you e-mail and password";
      }
    );
  }

  getUserData(){
    this.authService.getCredentialData(sessionStorage.getItem('token')|| '').subscribe(
      result => {
        this.router.navigate([this.returnUrl]);
      },
      err =>{
        this.messageRespond = "An error has occurred while getting login data, try again";
      },
    )
  }

  restorePassword(){
    console.log("request restore password")
    this.router.navigate([this.forgotPassword]);
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email] ],
      password: ['', [Validators.required/*, Validators.minLength(this.vs.formRules.passwordMin), Validators.pattern(this.vs.formRules.passwordPattern)*/] ],
    })
  }
}
