import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { SessionState, User } from '../Data/Credentials';

import { Room } from '../Data/Room';
import { CredentialsService } from '../services/credentials.service';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements AfterViewInit {
  credentials: SessionState;
  userData: User = new User();

  rooms: Room[] = [{ name: "testing name", description: "testing box for description", _id: 1, n_bed: 2, mediaLink: 'testing media link' }];

  displayedColumns: string[] = ['_id', 'name', 'n_bed', 'mediaLink', 'actions'];
  dataSource = new MatTableDataSource<Room>(this.rooms);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  addRoomForm: FormGroup = new FormGroup({});

  constructor(
    private route: ActivatedRoute,
    private authService: CredentialsService,
    private roomService: RoomsService,
    private fb: FormBuilder,
  ) {
    let userId = Number.parseInt(this.route.snapshot.paramMap.get('user_id') || '-1');
    this.credentials = this.authService.logInfo;
    /// Get user personal info
    this.authService.getUserDetails(userId.toString()).subscribe(
      res => {
        this.userData = res;
      },
      err => {
        console.log("error getting user details");
      }
    );
    if(this.credentials.role_value != '2')
      return;
    this.createForm();
  }

  ngAfterViewInit() {
    this.fetchData();
  }

  fetchData() {
    this.roomService.getRooms().subscribe(
      res => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  createForm() {
    this.addRoomForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required/*, Validators.minLength(this.vs.formRules.passwordMin), Validators.pattern(this.vs.formRules.passwordPattern)*/]],
      n_bed: ['', Validators.required],
      mediaLink: ['', Validators.required],
    })
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.addRoomForm.invalid) {
      return;
    }
    this.sendRequest();
  }

  removeRoom(roomid: string) {
    this.roomService.removeRoom(roomid).subscribe(
      done =>{
        console.log('room removed');
      }
    );
  }

  sendRequest() {
    let roomData = new Room(
      this.addRoomForm.value.name,
      this.addRoomForm.value.n_bed,
      this.addRoomForm.value.description,
      this.addRoomForm.value.mediaLink,
    )
    this.dataSource.data.push(roomData);
    this.roomService.submitRoom(roomData).subscribe(
      done => {
        if (done) {
          this.addRoomForm.reset();
          console.log("room added successfuly");
        }
      },
    );
  }
}
