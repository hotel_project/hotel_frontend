import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CredentialsService } from '../services/credentials.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  LoginInfo() {
    return this.authService.logInfo;
  }

  isLogged(){
    return this.authService.logInfo.logged;
  }

  UserId(){
    return this.authService.credential._id;
  }

  @Output() sidenavClose = new EventEmitter();
  constructor(private authService: CredentialsService) { }

  ngOnInit() {
  }
  
  onLogOut(){
    this.authService.logout();
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }
}
