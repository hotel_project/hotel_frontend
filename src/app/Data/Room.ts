export class Room{
    public _id: number = 0;
    public name : string = 'Testing Room';
    public n_bed :number = 0;
    public description : string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    public mediaLink : string = 'https://images.pexels.com/photos/2507007/pexels-photo-2507007.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';

    constructor(n?: string, b?:number, d?: string, m?: string){
        this.name = n || "";
        this.description = d || "";
        this.n_bed = b || -1;
        this.mediaLink = m || "";
    }
}

export class Booking{
    public d_start: string = '';
    public d_end: string = '';
    public id_room : number = -1;
    public id_client : number = -1;

    constructor(s_date: string, e_date: string, r_id : number, u_id: number){
        this.d_start = s_date;
        this.d_end = e_date;
        this.id_room = r_id;
        this.id_client = u_id;
    }
}
