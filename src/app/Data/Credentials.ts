export class Credential{
    public _id: number = 0;
    public email : string = '';
    public password: string = '';
    public type: string = '-1';
}

export class User{
    public _id : number = 0;
    public name : string = '';
    public surname : string = '';
    public cf : string = '';
}

export class SessionState{
    public logged:boolean = false; 
    public email: string = "";
    public role_value : string = '';
    constructor(log : boolean, email : string, role : string){
        this.logged = log;
        this.email = email;
        this.role_value = role;
    }
}
