import { Component, Input, OnInit } from '@angular/core';
import { Route, RouterLink } from '@angular/router';

import { Room } from '../Data/Room';

@Component({
  selector: 'app-room-card',
  templateUrl: './room-card.component.html',
  styleUrls: ['./room-card.component.less']
})
export class RoomCardComponent implements OnInit {

  @Input() roomData : Room = new Room();

  constructor() { }

  ngOnInit(): void {
  }
}
