import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  slides = [
    {'image': '../assets/hotel_1.jpg'}, 
    {'image': '../assets/hotel_2.jpg'},
    {'image': '../assets/hotel_3.jpg'},
    {'image': '../assets/hotel_4.jpg'}, 
    {'image': '../assets/hotel_5.jpg'}
  ];

  rooms: any = []

  constructor(
    private roomService : RoomsService
  ) { }

  ngOnInit(): void {
    this.getRooms();
  }

  getRooms() : void {
    this.roomService.getHigtLightRoom().subscribe(
      res=>{
        this.rooms = res;
      }
    )
  }
}
