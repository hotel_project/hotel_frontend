import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InfoComponent } from './info/info.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RoomDetailsComponent } from './room-details/room-details.component';
import { RoomListComponent } from './room-list/room-list.component';
import { RoleGuardService } from './services/roleguard.service';
import { SigninComponent } from './signin/signin.component';

const routes: Routes = [
  {path: "", redirectTo: "/home", pathMatch: "full"},
  {path: "home", component: HomeComponent},
  {path: "login", component: LoginComponent},
  {path: "room_list", component: RoomListComponent},
  {path: "room_inspector/:roomId", component: RoomDetailsComponent},
  {path: "who_we_are", component: InfoComponent},
  {path: "sign-in", component : SigninComponent},
  {path: "profile/:user_id", canActivate:[RoleGuardService], component : ProfileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
