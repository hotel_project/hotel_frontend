import { EventEmitter, Injectable, Output, ɵɵsetComponentScope } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, first, catchError, tap } from 'rxjs/operators'
import { environment } from '../../environments/environment'
import { Router } from '@angular/router';
import { Credential, SessionState, User } from  '../Data/Credentials';

@Injectable({
  providedIn: 'root'
})
export class CredentialsService {
  public credential: Credential = new Credential();

  logInfo = {logged: false, email: "", role_value : '-1'};
  private updateInterval : any = null;
  private refreshTime = 1000 * 60 * 1;

  constructor(private http: HttpClient, private router: Router)
  {
    this.refreshLoggedStateCycle(null);
  }

  login(email: string, password: string): Observable<boolean>{
    return this.http.post<{token: string}>(environment.serverUrl + 'login', {email: email, password: password})
    .pipe(
      map(
        result => {
          sessionStorage.setItem('token', result.token);
          this.refreshLoggedStateCycle({logged: true, email: email, role_value: 1});
          return true;
        },
      )
    )
  }

  submitRegistration(data: Object) : Observable<any> {
    return this.http.post<any>(`${environment.serverUrl}add_user`, data);
  }


  getCredentialData(token: string): Observable<Credential>{
    return this.http.post<Credential>(environment.serverUrl + 'get_user_by_token', {token: token})
    .pipe(
      map(
        result => {
          this.credential = result;
          return result;
        },
      )
    )
  }

  getUserDetails(userId: string) : Observable<User>{
    let data = {_id : userId}
    return this.http.post<User>(`${environment.serverUrl}get_user_details`, data);
  }


  isLogged(): Observable<boolean>{
    return this.requestLoggedState().pipe(map(state => {
      this.updateLoggedState(state);
      return state["logged"];
    }), first());
  }

  logout(): void {
    this.refreshLoggedStateCycle({logged: false, email: "", role_value :-1});
    this.router.navigate([this.router.url]);
    this.router.navigate(['login']);
  }

  refreshLoggedStateCycle(state: any = null): void {
    if (state == null)
      this.requestLoggedState().subscribe((e) => { this.updateLoggedState(e); });
    else {
      this.updateLoggedState(state);
    }

    if (this.updateInterval != null) {
      clearInterval(this.updateInterval);
      this.updateInterval = null;
    }

    this.updateInterval = setInterval(() => {
      this.requestLoggedState().subscribe((e) => this.updateLoggedState(e));
    }, this.refreshTime);
  }

  private updateLoggedState(newState : SessionState) : void {
    if (newState.logged == false)
    {
      if (sessionStorage.getItem('token') != null)
      {
        this.logInfo = newState;
        sessionStorage.removeItem('token');
      }
    }
    else{
      this.logInfo = newState;
      if(this.credential.email == ''){
        this.getCredentialData(sessionStorage.getItem('token') || '').subscribe();
      }
    }
  }

  private requestLoggedState() : Observable<any> {
    let token = sessionStorage.getItem('token');
    // console.log("TOKEN-" + token);
    if (!token)
    {
      // console.log("No token");
      return of(JSON.stringify( {logged: false, email: "", id_user_type : undefined} ));
    }
    return this.http.get<any>(environment.serverUrl + 'verify?token=' + token);
  }

  /**
   * returns null if there is no token
   */
  getToken() : string{
    return sessionStorage.getItem('token') || '';
  }
}
