import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Booking, Room } from '../Data/Room';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  constructor(private http: HttpClient) { }

  getRooms() : Observable<Room[]> {
    return this.http.get<Room[]>(`${environment.serverUrl}get_rooms`);
  }

  getRoom(room_id: Number): Observable<Room> {
    return this.http.get<Room>(`${environment.serverUrl}get_room?_id=${room_id}`, );
  }

  submitBooking(bookingData: Booking) : Observable<any>{
    return this.http.post<any>(`${environment.serverUrl}submit_booking`, bookingData);
  }
  getRoomReservations(roomId:Number): Observable<Booking[]>{
    return this.http.get<Booking[]>(`${environment.serverUrl}get_room_reservations?_id=${roomId}`);
  }

  getHigtLightRoom() : Observable<Room[]>{
    return this.http.get<Room[]>(`${environment.serverUrl}get_first_room`);
  }

  submitRoom(roomData: Room) : Observable<boolean> {
    return this.http.post<boolean>(`${environment.serverUrl}add_room`, roomData);
  }
  removeRoom(roomId: string) : Observable<any> {
    let data = {
      '_id': roomId
    }
    return this.http.post<any>(`${environment.serverUrl}remove_room`, data);
  }
}
