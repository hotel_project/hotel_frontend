// src/app/auth/role-guard.service.ts
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CredentialsService } from './credentials.service';

@Injectable({
  providedIn: 'root'
})

export class RoleGuardService implements CanActivate {
  constructor(public auth: CredentialsService, public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.auth.isLogged().pipe(tap(loggedIn => {
      console.log("userRole " ,this.auth.logInfo)
      if (!loggedIn)
        this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
      else{

        const expectedRole = route.data.expectedRole;
        const userRole = this.auth.logInfo.role_value;

        if ( expectedRole != undefined && (expectedRole != userRole && (Array.isArray(expectedRole) &&!expectedRole.includes(userRole)))) {
          console.log(" NON SEI AUTORIZZATO , VAI A dashboard");
          this.router.navigate(['dashboard']);
        }
      }
    }));
  }
}
