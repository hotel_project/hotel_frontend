import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmedValidator } from 'src/utils/utils';
import { CredentialsService } from '../services/credentials.service';
import { ValidationService } from '../services/validation.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.less']
})
export class SigninComponent implements OnInit {

  messageRespond : String = "";
  signin : FormGroup = new FormGroup({});
  returnUrl: string = "home";
  forgotPassword : string = "forgot-password"
  formErrors: any;
  submitted = false;

  // convenience getter for easy access to form fields
  get f() { return this.signin.controls; }

  get controller() { return this.signin}

  constructor(
    private router: Router,
    private fb: FormBuilder,
    public vs: ValidationService,
    private route: ActivatedRoute,
    private authService: CredentialsService
  ) { }

  ngOnInit(): void {
    let tmpReturnUrl = this.route.snapshot.queryParams["returnUrl"];
    if (tmpReturnUrl != undefined)
    {
      this.returnUrl = tmpReturnUrl;
    }
    this.formErrors = this.vs.errorMessages;
    this.createForm();
  }

  onSubmit(){
    this.submitted = true;
    this.signin.markAllAsTouched();
    // stop here if form is invalid
    if (this.signin.invalid) {
      if(this.signin.errors){
        console.log(this.f.confirm_password.errors);
      }
      return;
    }

    this.sendRequest();
    this.messageRespond = "";
  }

  sendRequest (){
    let userData = {
      'email': this.signin.value.email,
      'password' : this.signin.value.password,
      'name' : this.signin.value.name,
      'surname' : this.signin.value.surname,
      'cf' : this.signin.value.cf,
    }

    this.authService.submitRegistration(userData).subscribe(
      res =>{
        console.log("signin done");
        this.messageRespond = "Successfuly submitted";
      },
      err=>{
        this.messageRespond = err;
      }
    )
  }

  restorePassword(){
    console.log("request restore password")
    this.router.navigate([this.forgotPassword]);
  }

  createForm() {
    this.signin = this.fb.group({
      email: ['', [Validators.required, Validators.email] ],
      password: ['', [Validators.required/*, Validators.minLength(this.vs.formRules.passwordMin), Validators.pattern(this.vs.formRules.passwordPattern)*/] ],
      confirm_password : ['', Validators.required],
      name: ['',Validators.required],
      surname: ['',Validators.required],
      cf: ['',Validators.required],
    },{ 
      validator: ConfirmedValidator('password', 'confirm_password')
    })
  }
}
