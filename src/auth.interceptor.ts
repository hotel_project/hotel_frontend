import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = sessionStorage.getItem('token');
    if (token != null){
      const cloned = request.clone({
        headers: request.headers.set("Authorization", "Bearer " + token)
      });
      console.log("c'è un token");
      return next.handle(cloned);
    }
    else
    {
      console.log("niente token");
      return next.handle(request);
    }
  }
}
